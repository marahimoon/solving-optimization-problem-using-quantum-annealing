import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import math
from itertools import combinations
from random import random, choice
from random import *




def ER(n, p):
    V = set([v for v in range(n)])
    E = set()
    for combination in combinations(V, 2):
        a = random()
        if a < p:
            E.add(combination)

    g = nx.Graph()
    g.add_nodes_from(V)
    g.add_edges_from(E)

    return g


n = 5
p = 0.75
GG = ER(n, p)
#print(len(GG.edges()))





def random_dag(nodes, edges):
    """Generate a random Directed Acyclic Graph (DAG) with a given number of nodes and edges."""
    G1 = nx.DiGraph()
    for i in range(nodes):
        G1.add_node(i)
    while edges > 0:
        a = randint(0,nodes-1)
        b=a
        while b==a:
            b = randint(0,nodes-1)
        G1.add_edge(a,b)
        if nx.is_directed_acyclic_graph(G1):
            edges -= 1
        else:
            # we closed a loop!
            G1.remove_edge(a,b)
    nx.write_edgelist(G1, 'graph.txt')
    return G1

#G = random_dag(5,30)



def Capacity(G):
		
	capacity = np.array([math.ceil(10 * np.random.random()) for (i, j) in G.edges])
	np.savetxt("capacity.txt", capacity, fmt='%i')



def Cost(G, T_commodities):

	cost = [[math.ceil(10 * np.random.random()) for i in range(1, len(G.edges) + 1)] for k in range(T_commodities)]
	np.savetxt("cost.txt", cost, fmt='%i')



def Commodity_demand(T_commodities):

	comm_dem = [math.ceil(5) for k in range(T_commodities)]
	np.savetxt("comm_dem.txt", comm_dem, fmt='%i')

	




