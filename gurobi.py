import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
from gurobipy import Model, GRB, quicksum
import time




class GUROBI():

	def __init__(self, T_commodities, source_nodes, dest_nodes, sampler):
		
		self.T_commodities = T_commodities
		self.source_nodes = source_nodes
		self.dest_nodes = dest_nodes
		self.sampler = sampler


		self.time = None

		self.mdl =  Model('IMC_QUBO_Exact_solver')
		self.timing = {}

		self.clock = time.time() # Begin stopwatch
		self.formulate() # formulate problem
		self.timing['formulation_time'] = (time.time() - self.clock)
		


	
	def formulate(self):
		G = nx.read_edgelist('Instances/graph.txt', create_using = nx.DiGraph())
		#nx.draw(G)
		'''
			Define the binary variables X_i_j_k 
		                       
									'''

		x1 = [[('x_{0}_{1}_{2}'.format(i, j, k)) for (i, j) in G.edges] for k in range(self.T_commodities)]
		x = [self.mdl.addVars(x1[k], vtype=GRB.BINARY)for k in range(self.T_commodities)]



		'''  
		         Assign the demands to each commodity  
		                                                 
		                                                    '''
		comm_dem = np.loadtxt('Instances/comm_dem.txt')



		'''  
		         Define the the cost the cost matrix C for each arc link  
		                                                                    
		                                                                    '''

		cost = np.loadtxt('Instances/cost.txt')
		if cost.shape == (1,G.number_of_edges()):
		   cost = np.array([np.loadtxt('Instances/cost.txt')])



		'''  
		         Assign the cost to each variable link with commodity demands  
		                                                                       
		                                                                         '''

		sum_cost_x = sum([(np.matmul(list((x[k]['x_{0}_{1}_{2}'.format(i, j, k)]) for (i, j) in G.edges), cost[k]) * comm_dem[k]) for k in range(self.T_commodities)])


		'''  
		         Define the variables associate with edges which out from the 
		         nodes i and the edges which into the nodes i.  
                                                                    
		                                                                        '''

		inn = [[[(x[k]['x_{0}_{1}_{2}'.format(i, j, k)]) for (i, j) in G.in_edges(str(nodes))] for nodes in
      range(G.number_of_nodes())] for k in range(self.T_commodities)]

		out = [[[(x[k]['x_{0}_{1}_{2}'.format(i, j, k)]) for (i, j) in G.out_edges(str(nodes))] for nodes in
      range(G.number_of_nodes())] for k in range(self.T_commodities)]




#---------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------

		'''
                             
		                               The constraints of the problem
		                    
		                                                                          '''
                                                                            

		# sum of out nodes - sum of in nodes = b_k...  balanced constraint

			
		'''

		         sum of supply and demand nodes. or nodes with source or destination points... 
		         sum((sum_out - sum_in - d) ** 2) or sum((sum_out - sum_in + d) ** 2)
                                		                     
				
												 '''
		diff_k_out_in_s, diff_k_out_in_d, diff_k_out_in = [] , [], []
		a = 0

		for k in range(self.T_commodities):

        		diff_out_in_s, diff_out_in_d, diff_out_in = [] , [], []
        		a1, b1, c1 = 0, 0, 0

        		for nodes in range(G.number_of_nodes()):
	
            			sum_in, sum_out = 0, 0
            			sum_in = sum(inn[k][nodes])
            			sum_out = sum(out[k][nodes])
	
            			if nodes in self.source_nodes[k]:
	
               				self.mdl.addConstr(sum_out - sum_in == 1)
               				a1 += (sum_out - sum_in - 1)**2

            			if nodes in self.dest_nodes[k]:

               				self.mdl.addConstr(sum_out - sum_in == -1)
               				b1 += (sum_out - sum_in + 1)**2
             
            			if nodes not in list(set(self.dest_nodes[k])) and nodes not in list(set(self.source_nodes[k])):

               				self.mdl.addConstr(sum_out - sum_in == 0)
               				c1 += (sum_out - sum_in )**2
             
            			balanced_panalty = a1+b1+c1
			
			



		'''  

			Capacity constraint with slack variables
          
                        	      
                        		                                 '''

		capcit = np.loadtxt('Instances/capacity.txt')
		capacity = {(i, j): int(capcit[edges]) for (i, j),edges in zip(G.edges, range(G.number_of_edges()))}
		cap=[sum([x[k]['x_{0}_{1}_{2}'.format(i, j, k)]*comm_dem[k] for k in range(self.T_commodities)]) for (i, j) in G.edges()]

		s = [[('s_{0}_{1}_{2}'.format(i, j, m)) for m in range(capacity[(i, j)])] for (i, j) in G.edges]
		slack_variables = [self.mdl.addVars(s[i], vtype=GRB.BINARY)for i in range(G.number_of_edges())]    		
		self.mdl.update()
		capacity_penalty = sum([(-cap[l] + sum(slack_variables[l].values()))**2 for l in range(G.number_of_edges())])

	

		self.mdl.addConstrs(cap[l] <= capacity[(i, j)] for l, (i, j) in zip(range(G.number_of_edges()), G.edges()))
		





#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------
		'''                           
                                         	Gurobi Solver
		
	                                                                      '''



		
		self.mdl.modelSense = GRB.MINIMIZE
		  
		if self.sampler == 'GUROBI':
			self.mdl.setObjective(sum_cost_x)
			self.mdl.optimize()  # Gurobi Optimizer
		              
		if self.sampler == 'GUROBI_QUBO':
			self.mdl.setObjective(sum_cost_x + 2*10*10*balanced_panalty + 10*capacity_penalty )
			#mdl.Params.MIPGap = 0.1    # Gap% reach when terminate
			self.mdl.Params.TimeLimit = 120  # seconds or after this time it terminate
			self.mdl.optimize()  # Gurobi Optimizer
		self.activeKeys = [[a for a in x1[k] if x[k][a].x >0.99] for k in range(self.T_commodities) ]


